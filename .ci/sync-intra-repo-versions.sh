#!/bin/bash

set -e

# https://stackoverflow.com/questions/59895/how-to-get-the-source-directory-of-a-bash-script-from-within-the-script-itself#answer-53183593
DIR="$( realpath $( dirname "${BASH_SOURCE[0]}") )"

# Same args as "toml set": file name, key, value
toml_set() {
    NEW_CONTENTS=$(toml set "$1" $2 "$3")
    echo "$NEW_CONTENTS" > "$1"
}

MASTER_TOML="$DIR/../zkplmt/Cargo.toml"
HELPERS_TOML="$DIR/../zkplmt-test-helpers/Cargo.toml"

MASTER_VERSION=$(toml get "$MASTER_TOML" package.version | tr -d \")
toml_set "$HELPERS_TOML" package.version "$MASTER_VERSION"
