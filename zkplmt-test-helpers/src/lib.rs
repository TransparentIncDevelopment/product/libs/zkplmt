use proptest::{
    collection::vec,
    prelude::{
        prop::test_runner::{RngAlgorithm, TestRng},
        RngCore, *,
    },
};

use rand::{CryptoRng, Error};

pub use zkplmt::core::verify_zkplmt;
use zkplmt::models::{CurveVector, Proof, VectorTuple};

prop_compose! {
    pub fn arb_message()(bytes: Vec<u8>) -> Vec<u8> {
        bytes
    }
}

prop_compose! {
    pub fn arb_curve_vector()(bytes: [u8; 32]) -> CurveVector {
        let mut rng = FakeCsprng::from_seed(bytes);
        CurveVector::random(&mut rng)
    }
}

prop_compose! {
    pub fn arb_vector_tuple()(curve_vectors in vec(arb_curve_vector(), ..20)) -> VectorTuple {
        VectorTuple{ values: curve_vectors }
    }
}

prop_compose! {
    pub fn arb_tuples()(tuples in vec(arb_vector_tuple(), ..20)) -> Vec<VectorTuple> {
        tuples
    }
}

prop_compose! {
    pub fn arb_proof()(_bytes: [u8; 32], ) -> Proof {
        Proof::new(Vec::new(), Vec::new())
    }
}

/// The Dalek types require the `CryptoRng` trait for random number generators. While this may
/// be important for cryptographic safety in some cases, this prevents deterministic RNG
/// needed for proptesting. We are using a new-type which impl's the trait
/// to trick Dalek that the RNG is cryptographically secure.
#[derive(Debug)]
pub struct FakeCsprng(TestRng);
impl CryptoRng for FakeCsprng {}
impl RngCore for FakeCsprng {
    fn next_u32(&mut self) -> u32 {
        self.0.next_u32()
    }

    fn next_u64(&mut self) -> u64 {
        self.0.next_u64()
    }

    fn fill_bytes(&mut self, dest: &mut [u8]) {
        self.0.fill_bytes(dest)
    }

    fn try_fill_bytes(&mut self, dest: &mut [u8]) -> Result<(), Error> {
        self.0.try_fill_bytes(dest)
    }
}

impl FakeCsprng {
    pub fn from_seed(seed: [u8; 32]) -> Self {
        FakeCsprng(TestRng::from_seed(RngAlgorithm::ChaCha, &seed))
    }
}
