include:
  - local: '.ci/tpfs.yaml'

build:
  extends: .build-template

build-no-std:
  extends: .build-template
  script:
    - cargo build --no-default-features

test:
  extends: .test-template

lint:
  extends: .lint-template

audit:
  extends: .audit-template
  allow_failure: true

publish-beta:
  extends: .publish-beta-template

# Setup the https credentials for `cargo` commands to have access to update the `tpfs` crates index
.crates-https-auth: &crates-https-auth-before-script
  - echo -e "machine gitlab.com\nlogin gitlab-ci-token\npassword ${CI_JOB_TOKEN}" > ~/.netrc

.crates-ssh-auth: &crates-ssh-auth
  # Setup ssh agent
  - eval `ssh-agent`
  - chmod 0600 $DEPLOY_SSH_KEY
  - ssh-add $DEPLOY_SSH_KEY

.trace-job-begin: &trace-job-begin
  # Store the job's start time as a POSIX timestamp in a
  # temporary file for job-level tracing.
  - date +%s | tr -d '\n' | tee "/tmp/${CI_JOB_ID}.start"

.trace-job-end: &trace-job-end
  - trace-job.sh "$(cat /tmp/${CI_JOB_ID}.start)" $CI_JOB_NAME

.publish-live: &publish-live
  # $CRATES_IO_TOKEN is defined as GitLab group level variable: https://gitlab.com/groups/TransparentIncDevelopment/-/settings/ci_cd
  - trace-cmd.sh cargo-login cargo login $CRATES_IO_TOKEN
  - ./.ci/sync-intra-repo-versions.sh
  - ./.ci/echo-toml-diffs.sh
  # We must allow --allow-dirty for release builds, since we automatically rewrite the dependency version
  - trace-cmd.sh publish-zkplmt cargo publish --manifest-path ./zkplmt/Cargo.toml

.publish-live-helpers: &publish-live-helpers
  # $CRATES_IO_TOKEN is defined as GitLab group level variable: https://gitlab.com/groups/TransparentIncDevelopment/-/settings/ci_cd
  - trace-cmd.sh cargo-login cargo login $CRATES_IO_TOKEN
  - ./.ci/sync-intra-repo-versions.sh
  - ./.ci/echo-toml-diffs.sh
  # We must allow --allow-dirty for release builds, since we automatically rewrite the dependency version
  - trace-cmd.sh publish-zkplmt-test-helpers-crate cargo publish --allow-dirty --manifest-path ./zkplmt-test-helpers/Cargo.toml

publish:
  extends:
    - .shared-config
  only:
    refs:
      - master
  stage: publish
  before_script:
    - *trace-job-begin
    - *crates-https-auth-before-script
    - *crates-ssh-auth
  script:
    - *publish-live
  after_script:
    - *trace-job-end

publish-helpers:
  extends:
    - .shared-config
  only:
    refs:
      - master
  stage: publish
  before_script:
    - *trace-job-begin
    - *crates-https-auth-before-script
    - *crates-ssh-auth
  script:
    - *publish-live-helpers
  after_script:
    - *trace-job-end

sync-dependencies:
  extends: .synchronize-central-dependency-registry

upgrade-downstream-repos:
  extends: .upgrade-consuming-repos
  variables:
    UPSTREAM_CRATES: zkplmt zkplmt-test-helpers
  before_script:
    - export REPO_VERSION=$(toml get ./zkplmt/Cargo.toml package.version | tr -d \")