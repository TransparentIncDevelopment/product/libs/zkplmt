pub use curve25519_dalek;
//use proptest::bits::usize;
use rand::distributions::{Distribution, Uniform};
use rand::prelude::SliceRandom;

use crate::bulletproofs::Bases;
use crate::models::{CurveVector, Proof, VectorTuple};
use alloc::vec::Vec;
pub use curve25519_dalek::{
    constants::RISTRETTO_BASEPOINT_POINT,
    ristretto::{CompressedRistretto, RistrettoPoint},
    scalar::Scalar,
    traits::{IsIdentity, MultiscalarMul},
};
use rand::{CryptoRng, RngCore};
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha256, Sha512};

const MAX_TRANSACTION_OUTPUTS: usize = 16;

pub trait Hashable {
    fn write_bytes(&self, hasher: &mut Sha256);
}

impl<T: Hashable> Hashable for &T {
    fn write_bytes(&self, hasher: &mut Sha256) {
        (*self).write_bytes(hasher);
    }
}

impl Hashable for Scalar {
    fn write_bytes(&self, hasher: &mut Sha256) {
        hasher.input(self.as_bytes());
    }
}

impl Hashable for RistrettoPoint {
    fn write_bytes(&self, hasher: &mut Sha256) {
        hasher.input(self.compress().to_bytes());
    }
}

impl<T: Hashable, const N: usize> Hashable for [T; N] {
    fn write_bytes(&self, hasher: &mut Sha256) {
        for t in self {
            t.write_bytes(hasher);
        }
    }
}

impl<T: Hashable> Hashable for Vec<T> {
    fn write_bytes(&self, hasher: &mut Sha256) {
        for t in self {
            t.write_bytes(hasher);
        }
    }
}

impl Hashable for &[u8] {
    fn write_bytes(&self, hasher: &mut Sha256) {
        hasher.input(self);
    }
}
pub fn to_scalar_hash(hashables: &[&dyn Hashable]) -> Scalar {
    let mut hasher = Sha256::new();
    let mut hash = [0u8; 32];
    for h in hashables {
        h.write_bytes(&mut hasher);
    }

    hash.copy_from_slice(hasher.result().as_slice());
    Scalar::from_bytes_mod_order(hash)
}

pub fn compute_bases() -> Bases {
    Bases::new(
        compute_l(),
        RISTRETTO_BASEPOINT_POINT,
        MAX_TRANSACTION_OUTPUTS,
    )
}

pub fn compute_k() -> RistrettoPoint {
    hash_to_curve_point(b"Transparent")
}

pub fn compute_l() -> RistrettoPoint {
    hash_to_curve_point(b"Systems")
}

#[allow(non_snake_case)]
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CryptoSystemParameters {
    pub G: RistrettoPoint,
    pub K: RistrettoPoint,
    pub L: RistrettoPoint,
    pub bases: Bases,
}

impl CryptoSystemParameters {
    pub fn generate() -> Self {
        Self {
            G: RISTRETTO_BASEPOINT_POINT,
            K: compute_k(),
            L: compute_l(),
            bases: compute_bases(),
        }
    }
}

impl CurveVector {
    pub fn size() -> usize {
        32 * 2
    }

    pub fn fill_bytes(&self, buf: &mut [u8]) {
        let bytes = self.x.compress().to_bytes();
        copy(buf, &bytes);
        let bytes = self.y.compress().to_bytes();
        copy(&mut buf[32..], &bytes);
    }

    pub fn to_byte_array(self) -> [u8; 64] {
        let mut bytes = [0u8; 64];
        copy(&mut bytes[0..32], self.x.compress().as_bytes());
        copy(&mut bytes[32..64], self.y.compress().as_bytes());
        bytes
    }

    // TODO: This should just be `decode`
    pub fn from_slice(bytes: &[u8]) -> Option<CurveVector> {
        let x = CompressedRistretto::from_slice(&bytes[0..32]).decompress()?;
        let y = CompressedRistretto::from_slice(&bytes[32..64]).decompress()?;
        Some(CurveVector { x, y })
    }

    pub fn to_hash(self) -> [u8; 32] {
        let mut hash = [0u8; 32];
        let bytes = self.to_byte_array();

        let mut hasher = Sha256::new();
        hasher.input(&bytes[0..64]);

        hash.copy_from_slice(hasher.result().as_slice());
        hash
    }
}

impl VectorTuple {
    pub fn size(&self) -> usize {
        self.values.len() * CurveVector::size()
    }

    pub fn fill_bytes(&self, buf: &mut [u8]) {
        let size = CurveVector::size();
        for i in 0..self.values.len() {
            self.values[i].fill_bytes(&mut buf[size * i..]);
        }
    }
}

pub fn get_random_curve_point<R>(csprng: &mut R) -> RistrettoPoint
where
    R: RngCore + CryptoRng,
{
    let mut inputs = [0u8; 8];

    csprng.fill_bytes(&mut inputs);
    hash_to_curve_point(&inputs)
}

pub fn hash_to_curve_point(input: &[u8]) -> RistrettoPoint {
    let mut bytes = vec![0u8; input.len()];
    copy(&mut bytes, input);
    let mut hash = [0u8; 32];
    let mut source_hash = [0u8; 32];

    let mut hasher = Sha256::new();
    hasher.input(&bytes);
    source_hash.copy_from_slice(hasher.result().as_slice());

    loop {
        let mut hasher = Sha256::new();
        bytes.push(0);
        hasher.input(&bytes);
        hash.copy_from_slice(hasher.result().as_slice());

        //this is to avoid second preimage attacks.
        for i in 0..32 {
            hash[i] ^= source_hash[i];
        }

        let point = CompressedRistretto(hash);
        if let Some(ed_point) = point.decompress() {
            //multiplication by 8 is not required for Ristretto Point
            let group_point = ed_point;
            if !group_point.is_identity() {
                return group_point;
            }
        }
    }
}

pub fn copy<T: Copy>(target: &mut [T], source: &[T]) {
    assert!(target.len() >= source.len());
    target[..source.len()].clone_from_slice(source)
}

/// Sort the first parameter and do the same swaps in the second parameter
pub fn joint_quicksort<S: PartialOrd, T>(input: &mut [S], conjugate: &mut [T]) -> usize {
    assert_eq!(input.len(), conjugate.len());

    let mut index_of_first = 0;
    let mut stack = vec![(0, input.len() - 1)];
    while let Some(slice) = stack.pop() {
        let start = slice.0;
        let end = slice.1;
        let mut i = start;
        let mut j = end;
        let mut forward = true;
        while i < j {
            if input[i] > input[j] {
                if i == index_of_first {
                    index_of_first = j;
                } else if j == index_of_first {
                    index_of_first = i;
                }
                forward = !forward;
                input.swap(i, j);
                conjugate.swap(i, j);
            }
            if forward {
                i += 1;
            } else {
                j -= 1;
            }
        }

        //now the pivot is at i==j
        if i > 0 && start < i - 1 {
            stack.push((start, i - 1));
        }

        if end > i + 1 {
            stack.push((i + 1, end));
        }
    }

    index_of_first
}

/// Returns the index of the first element in the original slice in the final slice
pub fn shuffle<T, R: RngCore + CryptoRng>(input: &mut [T], csprng: &mut R) -> usize {
    input[1..].shuffle(csprng);
    let index = Uniform::from(0..input.len()).sample(csprng);
    input.swap(0, index);
    index
}

/// The first tuple must be the linear tuple. Shuffles the tuples vector.
pub fn create_zkplmt_shuffle<R>(
    tuples: &mut [VectorTuple],
    secret: Scalar,
    csprng: &mut R,
) -> Result<Proof, ZkPlmtError>
where
    R: RngCore + CryptoRng,
{
    let hidden_index = shuffle(tuples, csprng);
    create_zkplmt(&[], tuples, hidden_index, secret, csprng)
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Deserialize, Serialize)]
#[cfg_attr(feature = "std", derive(thiserror::Error))]
pub enum ZkPlmtError {
    #[cfg_attr(feature = "std", error("Tuples have different sizes"))]
    TupleSizeMismatch,
    #[cfg_attr(feature = "std", error("Empty tuple"))]
    TupleSizeZero,
    #[cfg_attr(feature = "std", error("Empty tuple list"))]
    EmptyTupleList,
    #[cfg_attr(feature = "std", error("Invalid proof"))]
    InvalidProof,
}
struct ZkPlmtHasher {
    size_of_message: usize,
    vectors_per_tuple: usize,
    size_of_tuples: usize,
    buf: Vec<u8>,
}
#[allow(non_snake_case)]
impl ZkPlmtHasher {
    fn new(message: &[u8], tuples: &[VectorTuple]) -> Result<Self, ZkPlmtError> {
        if tuples.is_empty() {
            return Err(ZkPlmtError::EmptyTupleList);
        }
        let size_of_message = message.len();
        let size_of_tuples = tuples[0].size() * tuples.len();
        let vectors_per_tuple = tuples[0].values.len();
        let size_of_Ls = 32 * tuples.len() * vectors_per_tuple;
        let mut buf = vec![0u8; size_of_message + size_of_Ls + size_of_tuples];

        if vectors_per_tuple == 0 {
            return Err(ZkPlmtError::TupleSizeZero);
        }
        for tuple in tuples.iter() {
            if tuple.values.len() != vectors_per_tuple {
                return Err(ZkPlmtError::TupleSizeMismatch);
            }
        }
        //add the message to the buffer
        copy(&mut buf, message);

        //add the input tuples to the buffer
        //leave space for the message and the previous tuples
        for j in 0..tuples.len() {
            tuples[j].fill_bytes(&mut buf[size_of_message + j * tuples[0].size()..]);
        }
        Ok(ZkPlmtHasher {
            size_of_message,
            vectors_per_tuple,
            size_of_tuples,
            buf,
        })
    }

    fn insert_L(&mut self, L: RistrettoPoint, i: usize, j: usize) {
        let bytes = L.compress().to_bytes();
        let target_index = self.size_of_message
            + self.size_of_tuples
            + (j * self.vectors_per_tuple + i) * bytes.len();
        copy(&mut self.buf[target_index..], &bytes);
    }

    fn hash(&self) -> Scalar {
        Scalar::hash_from_bytes::<Sha512>(&self.buf)
    }
}
/// TODO: It would be very helpful to have some documentation that maps between the more common
///   terms used in the monero/cryptonote papers, and the "zkplmt" terms. Importantly - how and
///   where do keys map to curve points / tuples.
///
/// Sign the `message` by creating a proof that the tuple found in `tuples` at `hidden_index` is a
/// linear tuple, given a `secret` Scalar (proportionality constant). This can only be done by
/// someone who owns the private key of the tuple at the hidden index.
#[allow(non_snake_case)]
pub fn create_zkplmt<R>(
    message: &[u8],
    tuples: &[VectorTuple],
    hidden_index: usize,
    secret: Scalar,
    csprng: &mut R,
) -> Result<Proof, ZkPlmtError>
where
    R: RngCore + CryptoRng,
{
    let mut hasher = ZkPlmtHasher::new(message, tuples)?;

    let vectors_per_tuple = tuples[0].values.len();
    let r = Scalar::random(csprng);
    let mut c = vec![Scalar::zero(); tuples.len()];
    let mut d = vec![Scalar::zero(); tuples.len()];
    let mut sum = Scalar::zero();
    for j in 0..tuples.len() {
        if j != hidden_index {
            c[j] = Scalar::random(csprng);
            d[j] = Scalar::random(csprng);
            for i in 0..vectors_per_tuple {
                let L = c[j] * tuples[j].values[i].x + d[j] * tuples[j].values[i].y;
                hasher.insert_L(L, i, j);
            }
            sum += d[j];
        }
    }
    for i in 0..vectors_per_tuple {
        let L = r * tuples[hidden_index].values[i].x;
        hasher.insert_L(L, i, hidden_index);
    }

    let hash_scalar = hasher.hash();
    d[hidden_index] = hash_scalar - sum;
    c[hidden_index] = r - d[hidden_index] * secret;

    Ok(Proof { c, d })
}

#[allow(non_snake_case)]
pub fn verify_zkplmt(
    message: &[u8],
    tuples: &[VectorTuple],
    proof: &Proof,
) -> Result<(), ZkPlmtError> {
    let mut mult_sc_vec = vec![Scalar::zero(); 2];
    let mut mult_ed_vec = vec![RistrettoPoint::default(); 2];

    let hasher_result = ZkPlmtHasher::new(message, tuples);

    if proof.c.len() != tuples.len() || proof.d.len() != tuples.len() {
        return Err(ZkPlmtError::InvalidProof);
    }

    match hasher_result {
        Ok(mut hasher) => {
            let c = &proof.c;
            let d = &proof.d;
            let vectors_per_tuple = tuples[0].values.len();

            let mut sum = Scalar::zero();

            for j in 0..tuples.len() {
                for i in 0..vectors_per_tuple {
                    mult_ed_vec[0] = tuples[j].values[i].x;
                    mult_ed_vec[1] = tuples[j].values[i].y;
                    mult_sc_vec[0] = c[j];
                    mult_sc_vec[1] = d[j];

                    let L = RistrettoPoint::multiscalar_mul(&mult_sc_vec, &mult_ed_vec);
                    hasher.insert_L(L, i, j);
                }
                sum += d[j];
            }

            let hash_scalar = hasher.hash();
            if hash_scalar.eq(&sum) {
                Ok(())
            } else {
                Err(ZkPlmtError::InvalidProof)
            }
        }
        Err(err) => Err(err),
    }
}

#[cfg(test)]
mod tests {
    use proptest::prelude::*;
    use zkplmt_test_helpers::{arb_message, arb_proof, arb_tuples, verify_zkplmt};

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(20))]

        #[test]
        fn verify_zkplmt_does_not_panic(
            message in arb_message(),
            tuples in arb_tuples(),
            proof in arb_proof(),
        ) {
            let _ = verify_zkplmt(&message, &tuples, &proof);
        }
    }
}
