/// Types that our cryptosystem depends on
use crate::core::{get_random_curve_point, RistrettoPoint, Scalar};
use alloc::vec::Vec;
use rand::{CryptoRng, RngCore};
use serde::{Deserialize, Serialize};

#[derive(Copy, Clone, Debug, Default, PartialEq, Eq, Deserialize, Serialize)]
pub struct CurveVector {
    pub x: RistrettoPoint,
    pub y: RistrettoPoint,
}

impl CurveVector {
    /// Generate a random CurveVector
    pub fn random<R>(csprng: &mut R) -> Self
    where
        R: RngCore + CryptoRng,
    {
        CurveVector {
            x: get_random_curve_point(csprng),
            y: get_random_curve_point(csprng),
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Deserialize, Serialize)]
pub struct Proof {
    pub(crate) c: Vec<Scalar>,
    pub(crate) d: Vec<Scalar>,
}

#[derive(Clone, Debug, PartialEq, Eq, Deserialize, Serialize)]
pub struct VectorTuple {
    pub values: Vec<CurveVector>,
}
/// Test helper to corrupt a valid proof by mutating it in place
#[cfg(any(test, feature = "test-helpers"))]
impl Proof {
    pub fn new(c: Vec<Scalar>, d: Vec<Scalar>) -> Self {
        Proof { c, d }
    }

    pub fn corrupt_proof(&mut self) {
        // Corrupt the last item on each vec
        let c_point_index = self.c.len() - 1;
        let d_point_index = self.d.len() - 1;
        corrupt_scalar(&mut self.c[c_point_index]);
        corrupt_scalar(&mut self.d[d_point_index]);
    }
}

#[cfg(any(test, feature = "test-helpers"))]
pub fn corrupt_scalar(scalar: &mut Scalar) {
    *scalar = scalar.invert();
}
