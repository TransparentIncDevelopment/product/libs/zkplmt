#![allow(non_snake_case)]

use alloc::vec::Vec;
use curve25519_dalek::{ristretto::*, scalar::Scalar, traits::MultiscalarMul};
use rand::{CryptoRng, RngCore};
use serde::{Deserialize, Serialize};

use crate::core::{copy, hash_to_curve_point, to_scalar_hash};
#[cfg(any(test, feature = "test-helpers"))]
use crate::models::corrupt_scalar;

pub const RANGE_SIZE: usize = 64;

struct DlTable {
    array: Vec<Vec<(CompressedRistretto, u64)>>,
}

impl DlTable {
    pub fn new() -> DlTable {
        let list = vec![Vec::<(CompressedRistretto, u64)>::new(); 1 << 20];
        DlTable { array: list }
    }

    pub fn insert(&mut self, V: CompressedRistretto, i: u64) {
        let b = V.as_bytes();
        let index = ((b[29] as usize & 0xf) << 16) | ((b[30] as usize) << 8) | (b[31] as usize);
        self.array[index].push((V, i));
    }

    pub fn get(&mut self, V: &CompressedRistretto) -> Option<u64> {
        let b = V.as_bytes();
        let index = ((b[29] as usize & 0xf) << 16) | ((b[30] as usize) << 8) | (b[31] as usize);
        let list = &self.array[index];
        for (W, i) in list {
            if *V == *W {
                return Some(*i);
            }
        }
        None
    }
}

pub fn discrete_log_2n_bit(n: u64, G: RistrettoPoint, H: RistrettoPoint) -> u64 {
    let two_to_the_n: u64 = 1 << n;
    let mut saved_map = DlTable::new();
    let mut V = RistrettoPoint::default();

    //Generate table
    for j in 0..two_to_the_n {
        let C = V.compress();
        saved_map.insert(C, j);
        V += G;
    }
    //V = Scalar::from(two_to_the_20) * G;
    let mut S = H;
    for i in 0..two_to_the_n {
        let C = S.compress();
        match saved_map.get(&C) {
            Some(j) => {
                return (i << n) + j;
            }
            None => {
                S -= V;
            }
        }
    }

    0
}

pub fn inner_product(a: &[Scalar], b: &[Scalar]) -> Scalar {
    assert_eq!(a.len(), b.len());
    let mut sum = Scalar::zero();
    for i in 0..a.len() {
        sum += a[i] * b[i];
    }
    sum
}

fn add_mult_ed(
    x: Scalar,
    a: &[RistrettoPoint],
    y: Scalar,
    b: &[RistrettoPoint],
) -> Vec<RistrettoPoint> {
    assert_eq!(a.len(), b.len());
    let mut output = vec![RistrettoPoint::default(); a.len()];
    for i in 0..a.len() {
        output[i] = x * a[i] + y * b[i];
    }
    output
}

fn mult_ed(x: &[Scalar], a: &[RistrettoPoint]) -> Vec<RistrettoPoint> {
    assert_eq!(x.len(), a.len());
    let mut output = vec![RistrettoPoint::default(); a.len()];
    for i in 0..a.len() {
        output[i] = x[i] * a[i];
    }
    output
}

// TODO: Unused
fn _mult_ed_single(x: Scalar, a: &[RistrettoPoint]) -> Vec<RistrettoPoint> {
    let mut output = vec![RistrettoPoint::default(); a.len()];
    for i in 0..a.len() {
        output[i] = x * a[i];
    }
    output
}

#[allow(clippy::many_single_char_names)]
pub fn multiscalar_mul_add(
    x: &[Scalar],
    a: &[RistrettoPoint],
    y: &[Scalar],
    b: &[RistrettoPoint],
) -> RistrettoPoint {
    let mut s = Vec::new();
    let mut v = Vec::new();
    for i in 0..x.len() {
        s.push(x[i]);
        v.push(a[i]);
    }
    for i in 0..y.len() {
        s.push(y[i]);
        v.push(b[i]);
    }
    RistrettoPoint::multiscalar_mul(&s, &v)
}

fn add_mult(x: Scalar, a: &[Scalar], y: Scalar, b: &[Scalar]) -> Vec<Scalar> {
    assert_eq!(a.len(), b.len());
    let mut output = vec![Scalar::zero(); a.len()];
    for i in 0..a.len() {
        output[i] = x * a[i] + y * b[i];
    }
    output
}

#[derive(Clone, Debug, PartialEq, Eq, Deserialize, Serialize)]
pub struct BulletProof {
    Ls: Vec<RistrettoPoint>,
    Rs: Vec<RistrettoPoint>,
    a: Scalar,
    b: Scalar,
}

#[allow(clippy::many_single_char_names)]
pub fn create_bulletproof(
    n: usize,
    g: &[RistrettoPoint],
    h: &[RistrettoPoint],
    u: RistrettoPoint,
    a: &[Scalar],
    b: &[Scalar],
) -> BulletProof {
    let mut a__ = a;
    let mut b__ = b;
    let mut Ls = Vec::new();
    let mut Rs = Vec::new();
    let mut G = g.to_vec();
    let mut H = h.to_vec();
    let mut n_ = n;
    let mut a_;
    let mut b_;
    let mut x_ = Scalar::zero();
    while n_ > 1 {
        let (L, R, temp_a_, temp_b_, x) =
            create_bulletproof_one_step(n_, &G, &H, &x_, &u, a__, b__);
        x_ = x;
        a_ = temp_a_;
        b_ = temp_b_;

        Ls.push(L);
        Rs.push(R);
        a__ = &a_;
        b__ = &b_;
        n_ /= 2;
        G = add_mult_ed(x.invert(), &G[0..n_], x, &G[n_..]);
        H = add_mult_ed(x, &H[0..n_], x.invert(), &H[n_..]);
    }

    BulletProof {
        Ls,
        Rs,
        a: a__[0],
        b: b__[0],
    }
}

#[allow(clippy::many_single_char_names)]
fn create_bulletproof_one_step(
    n: usize,
    g: &[RistrettoPoint],
    h: &[RistrettoPoint],
    hash_chain: &Scalar,
    u: &RistrettoPoint,
    a: &[Scalar],
    b: &[Scalar],
) -> (
    RistrettoPoint,
    RistrettoPoint,
    Vec<Scalar>,
    Vec<Scalar>,
    Scalar,
) {
    assert_eq!(n, g.len());
    assert_eq!(n, h.len());
    assert_eq!(n, a.len());
    assert_eq!(n, b.len());
    let n_ = n / 2;
    assert_eq!(n_ * 2, n);
    let a1 = &a[0..n_];
    let a2 = &a[n_..];
    let b1 = &b[0..n_];
    let b2 = &b[n_..];

    let L = multiscalar_mul_add(a1, &g[n_..], b2, &h[0..n_]) + inner_product(a1, b2) * u;
    let R = multiscalar_mul_add(a2, &g[0..n_], b1, &h[n_..]) + inner_product(a2, b1) * u;
    let x = to_scalar_hash(&[hash_chain, &L, &R]);
    let x_ = x.invert();
    let a_ = add_mult(x, a1, x_, a2);
    let b_ = add_mult(x_, b1, x, b2);

    (L, R, a_, b_, x)
}

fn alt_mult(x: Scalar, v: &mut [Scalar], b: usize) {
    let x_ = x.invert();
    let mut up = false;
    for (i, vv) in v.iter_mut().enumerate() {
        if i % b == 0 {
            up = !up;
        }
        if up {
            *vv *= x;
        } else {
            *vv *= x_;
        }
    }
}

#[allow(clippy::many_single_char_names)]
pub fn verify_bulletproof(
    n: usize,
    g: &[RistrettoPoint],
    h: &[RistrettoPoint],
    u: RistrettoPoint,
    P: RistrettoPoint,
    proof: &BulletProof,
) -> bool {
    let mut s = vec![Scalar::one(); n];
    let mut s_ = vec![Scalar::one(); n];
    let mut n_ = n;

    let mut Lmul = RistrettoPoint::default();
    let mut Rmul = RistrettoPoint::default();
    let Ls = &proof.Ls;
    let Rs = &proof.Rs;
    let a = proof.a;
    let b = proof.b;

    if Ls.len() != Rs.len() {
        return false;
    }

    let mut x = Scalar::zero();
    for i in 0..Ls.len() {
        let L = Ls[i];
        let R = Rs[i];

        x = to_scalar_hash(&[&x, &L, &R]);
        let x_ = x.invert();
        alt_mult(x_, &mut s, n_ / 2);
        alt_mult(x, &mut s_, n_ / 2);
        Lmul += x * x * L;
        Rmul += x_ * x_ * R;
        n_ /= 2;
    }

    if n_ != 1 {
        return false;
    }

    let G = RistrettoPoint::multiscalar_mul(s, g);
    let H = RistrettoPoint::multiscalar_mul(s_, h);
    let P_ = a * G + b * H + a * b * u;

    (P_) == P + Lmul + Rmul
}

#[allow(clippy::many_single_char_names)]
pub fn verify_bulletproof_hmul(
    n: usize,
    g: &[RistrettoPoint],
    h: &[RistrettoPoint],
    hmul: &[Scalar],
    u: RistrettoPoint,
    P: RistrettoPoint,
    proof: &BulletProof,
) -> bool {
    let mut s = vec![Scalar::one(); n];
    let mut s_ = vec![Scalar::one(); n];
    let mut n_ = n;

    let mut Lmul = RistrettoPoint::default();
    let mut Rmul = RistrettoPoint::default();
    let Ls = &proof.Ls;
    let Rs = &proof.Rs;
    let a = proof.a;
    let b = proof.b;
    if Ls.len() != Rs.len() {
        return false;
    }
    let mut x = Scalar::zero();
    for i in 0..Ls.len() {
        let L = Ls[i];
        let R = Rs[i];

        x = to_scalar_hash(&[&x, &L, &R]);
        let x_ = x.invert();
        alt_mult(x_, &mut s, n_ / 2);
        alt_mult(x, &mut s_, n_ / 2);
        Lmul += x * x * L;
        Rmul += x_ * x_ * R;
        n_ /= 2;
    }

    if n_ != 1 {
        return false;
    }
    let G = RistrettoPoint::multiscalar_mul(s, g);
    let H = RistrettoPoint::multiscalar_mul(multiply_scalar_arrays(&s_, hmul), h);
    let P_ = RistrettoPoint::multiscalar_mul(&[a, b, a * b], &[G, H, u]);

    (P_) == P + Lmul + Rmul
}

fn fill_random_scalars<R>(arr: &mut [Scalar], csprng: &mut R)
where
    R: RngCore + CryptoRng,
{
    for val in arr.iter_mut() {
        *val = Scalar::random(csprng);
    }
}

// TODO: Unused
fn _scalars_array_from_bits(v: u64) -> [Scalar; RANGE_SIZE] {
    let mut arr = [Scalar::default(); RANGE_SIZE];
    let mut w = v;
    for arr_val in arr.iter_mut().take(RANGE_SIZE) {
        *arr_val = Scalar::from(w & 1u64);
        w >>= 1;
    }
    arr
}

fn scalars_vec_from_bits_of_values_array(v: &[u64]) -> Vec<Scalar> {
    let m = v.len();
    let mut arr = vec![Scalar::default(); RANGE_SIZE * m];

    for j in 0..m {
        let mut w = v[j];
        for i in 0..RANGE_SIZE {
            arr[j * RANGE_SIZE + i] = Scalar::from(w & 1u64);
            w >>= 1;
        }
    }

    arr
}

// TODO: Unused
fn _add_scalar_to_array(a: &[Scalar], b: Scalar) -> Vec<Scalar> {
    let mut arr = vec![Scalar::default(); a.len()];
    for i in 0..a.len() {
        arr[i] = a[i] + b;
    }
    arr
}

fn multiply_scalar_to_array(a: &[Scalar], b: Scalar) -> Vec<Scalar> {
    let mut arr = vec![Scalar::default(); a.len()];
    for i in 0..a.len() {
        arr[i] = a[i] * b;
    }
    arr
}

fn array_of(z: Scalar, n: usize) -> Vec<Scalar> {
    let mut arr = vec![Scalar::default(); n];
    for val in arr.iter_mut().take(n) {
        *val = z;
    }
    arr
}

fn add_scalar_arrays(a: &[Scalar], b: &[Scalar]) -> Vec<Scalar> {
    let mut arr = vec![Scalar::default(); a.len()];
    for i in 0..a.len() {
        arr[i] = a[i] + b[i];
    }
    arr
}

fn substract_scalar_arrays(a: &[Scalar], b: &[Scalar]) -> Vec<Scalar> {
    let mut arr = vec![Scalar::default(); a.len()];
    for i in 0..a.len() {
        arr[i] = a[i] - b[i];
    }
    arr
}

pub fn multiply_scalar_arrays(a: &[Scalar], b: &[Scalar]) -> Vec<Scalar> {
    let mut arr = vec![Scalar::default(); a.len()];
    for i in 0..a.len() {
        arr[i] = a[i] * b[i];
    }
    arr
}

fn to_the_n(x: Scalar, n: usize) -> Vec<Scalar> {
    let mut arr = vec![Scalar::default(); n];
    let mut prod = Scalar::one();
    for val in arr.iter_mut().take(n) {
        *val = prod;
        prod *= x;
    }
    arr
}

fn to_the_n_multi_var(x: Scalar, z: Scalar, n: usize, m: usize) -> Vec<Scalar> {
    let mut arr = vec![Scalar::default(); n * m];
    let mut z_pow = z * z;
    for j in 0..m {
        let mut prod = z_pow;
        for i in 0..n {
            arr[j * RANGE_SIZE + i] = prod;
            prod *= x;
        }
        z_pow *= z;
    }

    arr
}

#[derive(Clone, Debug, PartialEq, Eq, Deserialize, Serialize)]
pub struct BulletRangeProof {
    A: RistrettoPoint,
    SS: RistrettoPoint,
    T1: RistrettoPoint,
    T2: RistrettoPoint,
    tao_x: Scalar,
    mu: Scalar,
    t_cap: Scalar,
    pub V: Vec<RistrettoPoint>,
    bullet_proof: BulletProof,
}

/// Test helper to corrupt a valid proof by mutating it in place
#[cfg(any(test, feature = "test-helpers"))]
impl BulletRangeProof {
    pub fn corrupt_proof(&mut self) {
        corrupt_scalar(&mut self.tao_x);
    }
}

pub fn bullet_range_verify(proof: &BulletRangeProof, bases: &Bases) -> bool {
    bullet_range_verify_ex(proof, bases, None, Scalar::zero())
}

#[allow(clippy::many_single_char_names)]
pub fn bullet_range_verify_ex(
    proof: &BulletRangeProof,
    bases: &Bases,
    extra_hash_input: Option<(&[u8], &RistrettoPoint, &RistrettoPoint)>,
    other_hash: Scalar,
) -> bool {
    let m = proof.V.len();
    let gs = bases.Gs[0..m * RANGE_SIZE].to_vec();
    let hs = bases.Hs[0..m * RANGE_SIZE].to_vec();
    let g = bases.GInit;
    let h = bases.HInit;
    let V = &proof.V;
    let x = match extra_hash_input {
        Some((message, R, P)) => {
            let mut points: Vec<&RistrettoPoint> = V.iter().collect();
            points.push(&proof.T1);
            points.push(&proof.T2);
            points.push(R);
            points.push(P);
            to_scalar_hash(&[&message, &points]) - other_hash
        }
        None => to_scalar_hash(&[V, &proof.T1, &proof.T2, &proof.A, &proof.SS]),
    };

    let y = to_scalar_hash(&[V, &proof.A, &proof.SS]);
    let z = to_scalar_hash(&[V, &proof.SS, &proof.A]);
    let ymn = to_the_n(y, RANGE_SIZE * m);
    let z_m = to_the_n(z, m);
    let twon = to_the_n(Scalar::from(2u64), RANGE_SIZE);
    let onen = to_the_n(Scalar::from(1u64), RANGE_SIZE);
    let one_m = to_the_n(Scalar::from(1u64), m);
    let mut _z2_z_m = multiply_scalar_to_array(&z_m, -z * z);
    let twon_z_pow = to_the_n_multi_var(Scalar::from(2u64), z, RANGE_SIZE, m);

    let one_mn = array_of(Scalar::from(1u64), RANGE_SIZE * m);
    let sig = (z - z * z) * inner_product(&one_mn, &ymn)
        - z * z * z * inner_product(&onen, &twon) * inner_product(&z_m, &one_m);

    let mut mult_scalars = vec![proof.t_cap, proof.tao_x, -sig, -x, -x * x];
    mult_scalars.append(&mut _z2_z_m);
    let mut mult_points = vec![g, h, g, proof.T1, proof.T2];
    mult_points.append(&mut proof.V.clone());

    let sum = RistrettoPoint::multiscalar_mul(
        &mut mult_scalars.into_iter(),
        &mut mult_points.into_iter(),
    );

    if sum != RistrettoPoint::default() {
        return false;
    }

    let y_mn = &to_the_n(y.invert(), RANGE_SIZE * m);

    let zymn = multiply_scalar_to_array(&ymn, z);
    let h__ = multiscalar_mul_add(
        &multiply_scalar_arrays(&twon_z_pow, y_mn),
        &hs,
        &multiply_scalar_arrays(&zymn, y_mn),
        &hs,
    );
    let g__ = RistrettoPoint::multiscalar_mul(array_of(-z, RANGE_SIZE * m), gs.clone());
    let P = proof.A + x * proof.SS + g__ + h__;

    let P_ = P - proof.mu * h + proof.t_cap * h;

    verify_bulletproof_hmul(RANGE_SIZE * m, &gs, &hs, y_mn, h, P_, &proof.bullet_proof)
}

fn to_2s_power(y: usize) -> usize {
    let mut num_ones = 0;
    let mut pows = 1usize;
    let mut x = y;
    while x > 0 {
        pows <<= 1;
        if x & 1 == 1 {
            num_ones += 1;
        }
        x >>= 1;
    }
    if num_ones == 1 {
        y
    } else {
        pows
    }
}

fn to_2s_pow_vec(v: &[u64]) -> Vec<u64> {
    let mut v: Vec<u64> = v.to_vec();
    let twos_pow = to_2s_power(v.len());
    v.resize(twos_pow, 0);
    v
}

fn to_2s_pow_scalar_vec(g: &[Scalar]) -> Vec<Scalar> {
    let mut g: Vec<Scalar> = g.to_vec();
    let twos_pow = to_2s_power(g.len());
    g.resize(twos_pow, Scalar::zero());
    g
}
#[allow(non_snake_case)]
pub fn bullet_range_proof<R>(
    gamma: &[Scalar],
    v: &[u64], //the length of v must be a power of 2 at this point.
    bases: &Bases,
    csprng: &mut R,
) -> BulletRangeProof
where
    R: RngCore + CryptoRng,
{
    bullet_range_proof_ex(gamma, v, bases, csprng)
}

// TODO: Extract returned type to a struct and remove type complexity allowance
#[allow(clippy::many_single_char_names, clippy::type_complexity)]
fn bullet_range_proof_ex_T1_T2<R>(
    gamma: &[Scalar],
    // Length of v must be a power of 2 at this point. In case of fake proof, this is a random value
    v: &[u64],
    bases: &Bases,
    csprng: &mut R,
) -> (
    RistrettoPoint,
    RistrettoPoint,
    Vec<Scalar>,
    Vec<Scalar>,
    Vec<Scalar>,
    Vec<Scalar>,
    Scalar,
    Scalar,
    Scalar,
    usize,
    Scalar,
    Scalar,
    Vec<RistrettoPoint>,
    Vec<RistrettoPoint>,
    RistrettoPoint,
    RistrettoPoint,
    Scalar,
    Vec<u64>,
    Vec<Scalar>,
)
where
    R: RngCore + CryptoRng,
{
    let v = to_2s_pow_vec(v);
    let gamma = to_2s_pow_scalar_vec(gamma);
    let m = v.len();
    let gs: Vec<RistrettoPoint> = bases.Gs[0..RANGE_SIZE * m].to_vec();
    let hs: Vec<RistrettoPoint> = bases.Hs[0..RANGE_SIZE * m].to_vec();
    let g = bases.GInit;
    let h = bases.HInit;

    let aL = scalars_vec_from_bits_of_values_array(&v);

    let aR = substract_scalar_arrays(&aL, &to_the_n(Scalar::from(1u64), RANGE_SIZE * m));

    let alpha = Scalar::random(csprng);
    let mut sL = vec![Scalar::default(); RANGE_SIZE * m];
    let mut sR = vec![Scalar::default(); RANGE_SIZE * m];
    fill_random_scalars(&mut sL, csprng);
    fill_random_scalars(&mut sR, csprng);
    let rho = Scalar::random(csprng);

    let A = alpha * h + multiscalar_mul_add(&aL, &gs, &aR, &hs);
    let SS = rho * h + multiscalar_mul_add(&sL, &gs, &sR, &hs);
    let V: Vec<RistrettoPoint> = v
        .clone()
        .into_iter()
        .zip(gamma.iter())
        .map(|(x, gam)| Scalar::from(x) * g + gam * h)
        .collect();

    let y = to_scalar_hash(&[&V, &A, &SS]);
    let z = to_scalar_hash(&[&V, &SS, &A]);

    let tao1 = Scalar::random(csprng);
    let tao2 = Scalar::random(csprng);
    let zmn = array_of(z, RANGE_SIZE * m);
    let l0 = substract_scalar_arrays(&aL, &zmn);
    let l1 = sL;

    let ymn = to_the_n(y, RANGE_SIZE * m);
    let twon_z_pow = to_the_n_multi_var(Scalar::from(2u64), z, RANGE_SIZE, m);

    let r1 = multiply_scalar_arrays(&ymn, &sR);
    let r0 = add_scalar_arrays(
        &multiply_scalar_arrays(&ymn, &add_scalar_arrays(&aR, &zmn)),
        &twon_z_pow,
    );
    let t1 = inner_product(&l0, &r1) + inner_product(&l1, &r0);
    let t2 = inner_product(&l1, &r1);
    let T1 = g * t1 + h * tao1;
    let T2 = g * t2 + h * tao2;
    (
        T1, T2, l0, r0, l1, r1, tao1, tao2, z, m, alpha, rho, gs, hs, A, SS, y, v, gamma,
    )
}

#[allow(clippy::too_many_arguments, clippy::many_single_char_names)]
fn bullet_range_proof_rest(
    bases: &Bases,
    T1: RistrettoPoint,
    T2: RistrettoPoint,
    l0: Vec<Scalar>,
    r0: Vec<Scalar>,
    l1: Vec<Scalar>,
    r1: Vec<Scalar>,
    tao1: Scalar,
    tao2: Scalar,
    z: Scalar,
    m: usize,
    alpha: Scalar,
    rho: Scalar,
    gs: Vec<RistrettoPoint>,
    hs: Vec<RistrettoPoint>,
    A: RistrettoPoint,
    SS: RistrettoPoint,
    y: Scalar,
    v: Vec<u64>,
    gamma: &[Scalar],
    challenge: Scalar,
) -> BulletRangeProof {
    let g = bases.GInit;
    let h = bases.HInit;
    let x = challenge;
    let l = add_scalar_arrays(&l0, &multiply_scalar_to_array(&l1, x));
    let r = add_scalar_arrays(&r0, &multiply_scalar_to_array(&r1, x));

    let z_m = to_the_n(z, m);
    let tao_x = x * x * tao2 + x * tao1 + z * z * inner_product(&z_m, gamma);

    // let t0 = inner_product(&l0, &r0);

    let t_cap = inner_product(&l, &r);

    //check t_cap is computed correctly.
    //assert_eq!(t2*x*x+t1*x+t0, t_cap);

    let mu = alpha + rho * x;
    //assert_eq!(g,h);
    let V: Vec<RistrettoPoint> = v
        .into_iter()
        .zip(gamma.iter())
        .map(|(x, gam)| Scalar::from(x) * g + gam * h)
        .collect();

    let h_ = mult_ed(&to_the_n(y.invert(), RANGE_SIZE * m), &hs);
    let bullet_proof = create_bulletproof(RANGE_SIZE * m, &gs, &h_, h, &l, &r);

    BulletRangeProof {
        A,
        SS,
        T1,
        T2,
        tao_x,
        mu,
        t_cap,
        V,
        bullet_proof,
    }
}

#[allow(non_snake_case)]
pub fn bullet_range_proof_ex<R>(
    gamma: &[Scalar],
    v: &[u64], //the length of v must be a power of 2 at this point. In case of fake proof, this is a random value
    bases: &Bases,
    csprng: &mut R,
) -> BulletRangeProof
where
    R: RngCore + CryptoRng,
{
    let g = bases.GInit;
    let h = bases.HInit;
    let (T1, T2, l0, r0, l1, r1, tao1, tao2, z, m, alpha, rho, gs, hs, A, SS, y, v, gamma) =
        bullet_range_proof_ex_T1_T2(gamma, v, bases, csprng);

    let V: Vec<RistrettoPoint> = v
        .clone()
        .into_iter()
        .zip(gamma.iter())
        .map(|(x, gam)| Scalar::from(x) * g + gam * h)
        .collect();

    let challenge = to_scalar_hash(&[&V, &T1, &T2, &A, &SS]);

    bullet_range_proof_rest(
        bases, T1, T2, l0, r0, l1, r1, tao1, tao2, z, m, alpha, rho, gs, hs, A, SS, y, v, &gamma,
        challenge,
    )
}

pub fn create_modified_schnorr<R>(
    bases: &Bases,
    message: &[u8],
    private_key: Scalar,
    extrapoints: (&RistrettoPoint, &RistrettoPoint),
    fake_hash: Scalar,
    csprng: &mut R,
) -> (Scalar, Scalar)
where
    R: RngCore + CryptoRng,
{
    let r = Scalar::random(csprng);
    let P = private_key * bases.GInit;
    let R = r * bases.GInit;
    let h = to_scalar_hash(&[&message, &extrapoints.0, extrapoints.1, &P, &R]) - fake_hash;
    let s = r - h * private_key;
    (s, h)
}

pub fn verify_modified_schnorr(
    bases: &Bases,
    message: &[u8],
    signature: &(Scalar, Scalar),
    P: &RistrettoPoint,
    extrapoints: (&RistrettoPoint, &RistrettoPoint),
    other_hash: Scalar,
) -> bool {
    let R = RistrettoPoint::multiscalar_mul(&[signature.0, signature.1], &[bases.GInit, *P]);
    let hash = to_scalar_hash(&[&message, extrapoints.0, extrapoints.1, P, &R]) - other_hash;
    hash.eq(&signature.1)
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
//The commitment V = v*G+gamma*H
pub struct Bases {
    GInit: RistrettoPoint,
    HInit: RistrettoPoint,
    Gs: Vec<RistrettoPoint>,
    Hs: Vec<RistrettoPoint>,
}

fn hash_of_points(x: &RistrettoPoint, y: &RistrettoPoint) -> RistrettoPoint {
    let mut buf = [0u8; 64];
    copy(&mut buf[0..32], x.compress().as_bytes());
    copy(&mut buf[32..64], y.compress().as_bytes());
    hash_to_curve_point(&buf)
}
impl Bases {
    pub fn new(GInit: RistrettoPoint, HInit: RistrettoPoint, max_len: usize) -> Bases {
        let mut ginit = GInit;
        let mut hinit = HInit;
        let mut gs = Vec::new();
        let mut hs = Vec::new();
        for _ in 0..RANGE_SIZE * max_len {
            ginit = hash_of_points(&ginit, &hinit);
            hinit = hash_of_points(&hinit, &ginit);
            gs.push(ginit);
            hs.push(hinit);
        }
        Bases {
            GInit,
            HInit,
            Gs: gs,
            Hs: hs,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::core::{compute_k, compute_l};
    use curve25519_dalek::constants::RISTRETTO_BASEPOINT_POINT;
    use rand::rngs::OsRng;

    lazy_static::lazy_static! {
        static ref BASE_K: RistrettoPoint = compute_k();
        static ref BASE_L: RistrettoPoint = compute_l();
        static ref BASE_G: RistrettoPoint = RISTRETTO_BASEPOINT_POINT;
    }

    #[allow(non_snake_case)]
    pub(crate) fn get_K() -> RistrettoPoint {
        *BASE_K
    }

    #[allow(non_snake_case)]
    pub(crate) fn get_L() -> RistrettoPoint {
        *BASE_L
    }

    #[allow(non_snake_case)]
    pub(crate) fn get_G() -> RistrettoPoint {
        *BASE_G
    }

    #[allow(non_snake_case)]
    #[test]
    fn test_bullet_range_proof() {
        let bases = Bases::new(get_L(), get_K(), 20);
        let mut csprng: OsRng = OsRng::default();
        let gamma = [
            Scalar::random(&mut csprng),
            Scalar::random(&mut csprng),
            Scalar::random(&mut csprng),
            Scalar::random(&mut csprng),
            Scalar::random(&mut csprng),
        ];
        let v = [
            csprng.next_u64(),
            csprng.next_u64(),
            csprng.next_u64(),
            csprng.next_u64(),
            csprng.next_u64(),
        ];

        let proof = bullet_range_proof(&gamma, &v, &bases, &mut csprng);
        assert!(bullet_range_verify(&proof, &bases));
    }

    #[test]
    fn test_bullet_range_proof_with_zero_value() {
        let bases = Bases::new(get_L(), get_K(), 20);
        let mut csprng: OsRng = OsRng::default();
        let gamma = [Scalar::random(&mut csprng)];
        let v = [0];

        let proof = bullet_range_proof(&gamma, &v, &bases, &mut csprng);
        assert!(bullet_range_verify(&proof, &bases));
    }

    // #[allow(non_snake_case)]
    // #[test]

    // fn test_discrete_log() {
    //     let n = 20u64;
    //     let mut csprng: OsRng = OsRng::new().unwrap();
    //     let rand = csprng.next_u64();
    //     let trimmed_rand = rand >> (RANGE_SIZE - 2 * n); // make a 2n bit random number
    //     let G = get_G();
    //     let H = Scalar::from(trimmed_rand) * G;
    //     let log = discrete_log_2n_bit(n, G, H);
    //     assert_eq!(log, trimmed_rand);
    // }

    #[allow(non_snake_case)]
    #[test]
    fn test_bulletproof() {
        let mut ginit = get_G();
        let mut hinit = get_K();
        let u = get_L();
        let mut gs = Vec::new();
        let mut hs = Vec::new();
        let mut a = Vec::new();
        let mut b = Vec::new();
        for _ in 0..16 {
            gs.push(to_scalar_hash(&[&ginit, &hinit]) * ginit);
            hs.push(to_scalar_hash(&[&hinit, &ginit]) * hinit);
            ginit = to_scalar_hash(&[&ginit, &hinit]) * ginit;
            hinit = to_scalar_hash(&[&hinit, &ginit]) * hinit;
            let mut csprng: OsRng = OsRng::default();
            a.push(Scalar::random(&mut csprng));
            b.push(Scalar::random(&mut csprng));
        }

        let P = multiscalar_mul_add(&a, &gs, &b, &hs) + inner_product(&a, &b) * u;
        let proof = create_bulletproof(16, &gs, &hs, u, &a, &b);

        let ver = verify_bulletproof(16, &gs, &hs, u, P, &proof);
        assert!(ver);
    }

    #[allow(non_snake_case)]
    #[test]
    fn test_modified_schnorr() {
        let mut csprng: OsRng = OsRng::default();
        let bases = Bases::new(get_L(), get_K(), 2);
        let private_key = Scalar::random(&mut csprng);
        let P = bases.GInit * private_key;
        let extra_points = (&bases.Gs[1], &bases.Hs[1]);
        let message = b"hello";
        let other_hash = Scalar::random(&mut csprng);
        let signature = create_modified_schnorr(
            &bases,
            message,
            private_key,
            extra_points,
            other_hash,
            &mut csprng,
        );
        assert!(verify_modified_schnorr(
            &bases,
            message,
            &signature,
            &P,
            extra_points,
            other_hash
        ));
    }

    #[allow(non_snake_case)]
    #[test]
    fn test_to_2s_power() {
        assert_eq!(to_2s_power(1), 1);
        assert_eq!(to_2s_power(3), 4);
        assert_eq!(to_2s_power(5), 8);
        assert_eq!(to_2s_power(8), 8);
    }
}
