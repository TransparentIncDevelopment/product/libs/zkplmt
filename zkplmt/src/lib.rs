#![crate_type = "lib"]
#![cfg_attr(not(feature = "std"), no_std)]

#[macro_use]
extern crate alloc;

pub mod bulletproofs;
/// Core primitives
pub mod core;
pub mod models;
pub mod verifiable_encryption;

#[cfg(test)]
mod tests {
    use super::core::*;
    use crate::models::{CurveVector, Proof, VectorTuple};
    use curve25519_dalek::scalar::Scalar;
    use rand::rngs::OsRng;
    use rand::{CryptoRng, RngCore};

    /// The first tuple must be the linear tuple. Shuffles the tuples vector.
    pub(crate) fn create_zkplmt_shuffle<R>(
        tuples: &mut [VectorTuple],
        secret: Scalar,
        csprng: &mut R,
    ) -> Result<Proof, ZkPlmtError>
    where
        R: RngCore + CryptoRng,
    {
        let hidden_index = shuffle(tuples, csprng);
        create_zkplmt(&[], tuples, hidden_index, secret, csprng)
    }
    #[test]
    fn test_zkplmt() {
        let mut csprng: OsRng = OsRng::default();
        let secret = Scalar::random(&mut csprng);
        let base_1 = get_random_curve_point(&mut csprng);
        let base_2 = get_random_curve_point(&mut csprng);
        let base_3 = get_random_curve_point(&mut csprng);

        let per_1 = secret * base_1;
        let per_2 = secret * base_2;
        let per_3 = secret * base_3;

        let curve_points_1 = vec![
            CurveVector {
                x: base_1,
                y: per_1,
            },
            CurveVector {
                x: base_2,
                y: per_2,
            },
            CurveVector {
                x: base_3,
                y: per_3,
            },
        ];

        let tuple_1 = VectorTuple {
            values: curve_points_1,
        };

        let mut tuples = vec![tuple_1];

        for _ in 1..15 {
            let base_1 = get_random_curve_point(&mut csprng);
            let base_2 = get_random_curve_point(&mut csprng);
            let base_3 = get_random_curve_point(&mut csprng);

            let per_1 = get_random_curve_point(&mut csprng);
            let per_2 = get_random_curve_point(&mut csprng);
            let per_3 = get_random_curve_point(&mut csprng);

            let curve_points_1 = vec![
                CurveVector {
                    x: base_1,
                    y: per_1,
                },
                CurveVector {
                    x: base_2,
                    y: per_2,
                },
                CurveVector {
                    x: base_3,
                    y: per_3,
                },
            ];

            let tuple_2 = VectorTuple {
                values: curve_points_1,
            };

            tuples.push(tuple_2);
        }

        let signature = create_zkplmt_shuffle(&mut tuples, secret, &mut csprng)
            .expect("It will never reach here");
        let result = verify_zkplmt(&[], &tuples, &signature);
        assert_eq!(result, Ok(()));
    }

    #[test]
    fn test_zkplmt_single() {
        let mut csprng: OsRng = OsRng::default();
        let secret = Scalar::random(&mut csprng);
        let base_1 = get_random_curve_point(&mut csprng);
        let base_2 = get_random_curve_point(&mut csprng);
        let base_3 = get_random_curve_point(&mut csprng);
        let base_4 = get_random_curve_point(&mut csprng);
        let base_5 = get_random_curve_point(&mut csprng);

        let per_1 = secret * base_1;
        let per_2 = secret * base_2;
        let per_3 = secret * base_3;
        let per_4 = secret * base_4;
        let per_5 = secret * base_5;

        let curve_points_1 = vec![
            CurveVector {
                x: base_1,
                y: per_1,
            },
            CurveVector {
                x: base_2,
                y: per_2,
            },
            CurveVector {
                x: base_3,
                y: per_3,
            },
            CurveVector {
                x: base_4,
                y: per_4,
            },
            CurveVector {
                x: base_5,
                y: per_5,
            },
        ];

        let tuple_1 = VectorTuple {
            values: curve_points_1,
        };

        let tuples = vec![tuple_1];

        let signature =
            create_zkplmt(&[], &tuples, 0, secret, &mut csprng).expect("It will never reach  here");
        let result = verify_zkplmt(&[], &tuples, &signature);
        assert_eq!(result, Ok(()));
    }

    #[test]
    fn test_zkplmt_fail() {
        let mut csprng: OsRng = OsRng::default();
        let secret = Scalar::random(&mut csprng);
        let base_1 = get_random_curve_point(&mut csprng);
        let base_2 = get_random_curve_point(&mut csprng);
        let base_3 = get_random_curve_point(&mut csprng);

        let per_1 = secret * base_1;
        let per_2 = secret * base_2;
        let per_3 = secret * base_3;

        let curve_points_1 = vec![
            CurveVector {
                x: base_1,
                y: per_1,
            },
            CurveVector {
                x: base_2,
                y: per_2,
            },
            CurveVector {
                x: base_3,
                y: per_3,
            },
        ];

        let tuple_1 = VectorTuple {
            values: curve_points_1,
        };

        let base_1 = get_random_curve_point(&mut csprng);
        let base_2 = get_random_curve_point(&mut csprng);
        let base_3 = get_random_curve_point(&mut csprng);

        let per_1 = get_random_curve_point(&mut csprng);
        let per_2 = get_random_curve_point(&mut csprng);
        let per_3 = get_random_curve_point(&mut csprng);

        let curve_points_1 = vec![
            CurveVector {
                x: base_1,
                y: per_1,
            },
            CurveVector {
                x: base_2,
                y: per_2,
            },
            CurveVector {
                x: base_3,
                y: per_3,
            },
        ];

        let tuple_2 = VectorTuple {
            values: curve_points_1,
        };

        let base_1 = get_random_curve_point(&mut csprng);
        let base_2 = get_random_curve_point(&mut csprng);
        let base_3 = get_random_curve_point(&mut csprng);

        let per_1 = get_random_curve_point(&mut csprng);
        let per_2 = get_random_curve_point(&mut csprng);
        let per_3 = get_random_curve_point(&mut csprng);

        let curve_points_1 = vec![
            CurveVector {
                x: base_1,
                y: per_1,
            },
            CurveVector {
                x: base_2,
                y: per_2,
            },
            CurveVector {
                x: base_3,
                y: per_3,
            },
        ];

        let tuple_3 = VectorTuple {
            values: curve_points_1,
        };

        let tuples = vec![tuple_2, tuple_1, tuple_3];

        let signature = create_zkplmt(&[], &tuples, 2usize, secret, &mut csprng)
            .expect("It will never reach here");
        let result = verify_zkplmt(&[], &tuples, &signature);
        assert_eq!(result, Err(ZkPlmtError::InvalidProof));
    }

    #[test]
    fn test_joint_quicksort() {
        let mut array = [1, 5, 2, 3, 1, 5];
        let mut conj = [0, 1, 2, 3, 4, 5];
        joint_quicksort(&mut array, &mut conj);
        assert_eq!(array, [1, 1, 2, 3, 5, 5]);
    }

    #[test]
    fn test_joint_quicksort_2() {
        let mut array = [5, 1, 4, 3, 2];
        let mut conj = [0, 1, 2, 3, 4];
        let index_of_first = joint_quicksort(&mut array, &mut conj);
        assert_eq!(array, [1, 2, 3, 4, 5]);
        assert_eq!(conj, [1, 4, 3, 2, 0]);
        assert_eq!(index_of_first, 4);
    }
}
