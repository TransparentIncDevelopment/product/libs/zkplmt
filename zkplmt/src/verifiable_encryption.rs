pub use curve25519_dalek;

use crate::core::ZkPlmtError;
use crate::core::{create_zkplmt, verify_zkplmt};
use crate::models::{CurveVector, Proof, VectorTuple};
use alloc::vec::Vec;
use core::ops::{Add, Mul};
#[cfg(any(test, feature = "test-helpers"))]
use curve25519_dalek::constants::RISTRETTO_BASEPOINT_POINT;
pub use curve25519_dalek::{
    ristretto::{CompressedRistretto, RistrettoPoint},
    scalar::Scalar,
    traits::{IsIdentity, MultiscalarMul},
};
use rand::{CryptoRng, RngCore};
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "std", derive(thiserror::Error))]
pub enum EncryptionVerificationError {
    #[cfg_attr(feature = "std", error("Verification failed"))]
    VerificationError,
    #[cfg_attr(feature = "std", error("zkplmt failed"))]
    FailedZkplmtError(#[cfg_attr(feature = "std", source)] ZkPlmtError),
}

impl From<ZkPlmtError> for EncryptionVerificationError {
    fn from(err: ZkPlmtError) -> Self {
        Self::FailedZkplmtError(err)
    }
}

#[allow(non_snake_case)]
#[derive(Clone, Debug, PartialEq, Eq, Deserialize, Serialize)]
pub struct VerifiableEncryptionOfCurvePoint {
    pub Y: RistrettoPoint,
    pub E: Vec<RistrettoPoint>,
    pub E_: Vec<RistrettoPoint>,
    pub X: Vec<RistrettoPoint>,
    pub B_: Vec<RistrettoPoint>,
    pub Q_: Vec<RistrettoPoint>,
    pub P_: RistrettoPoint,
    pub pi_1: Proof,
    pub pi_2: Proof,
}

fn vector_zip(x_values: &CurvePointList, y_values: &CurvePointList) -> Vec<CurveVector> {
    x_values
        .0
        .iter()
        .zip(y_values.0.iter())
        .map(|(e, e_)| CurveVector { x: *e, y: *e_ })
        .collect()
}

fn vector_unzip(vectors: &[CurveVector]) -> (CurvePointList, CurvePointList) {
    let x_values = CurvePointList(vectors.iter().map(|cp| cp.x).collect());
    let y_values = CurvePointList(vectors.iter().map(|cp| cp.y).collect());
    (x_values, y_values)
}

#[derive(Clone)]
struct CurvePointList(Vec<RistrettoPoint>);

impl Mul<Scalar> for &'_ CurvePointList {
    type Output = CurvePointList;

    fn mul(self, scalar: Scalar) -> Self::Output {
        self.clone() * scalar
    }
}

impl Mul<Scalar> for CurvePointList {
    type Output = CurvePointList;

    fn mul(self, scalar: Scalar) -> Self::Output {
        CurvePointList(self.0.into_iter().map(|point| scalar * point).collect())
    }
}

impl Add<&RistrettoPoint> for CurvePointList {
    type Output = CurvePointList;

    fn add(mut self, point2: &RistrettoPoint) -> Self::Output {
        self.0.iter_mut().for_each(|point| *point += point2);
        self
    }
}

impl From<Vec<RistrettoPoint>> for CurvePointList {
    fn from(v: Vec<RistrettoPoint>) -> Self {
        CurvePointList(v)
    }
}

#[allow(non_snake_case)]
impl VerifiableEncryptionOfCurvePoint {
    /**
    Creates a ZkPLMT tuple for the proof pi_1
    */
    fn setup_y_proof_tuple(
        G: &RistrettoPoint,
        Y: &RistrettoPoint,
        E: &CurvePointList,
        E_: &CurvePointList,
        B: &CurvePointList,
        B_: &CurvePointList,
    ) -> Vec<VectorTuple> {
        let Y_vector = CurveVector { x: *G, y: *Y };
        let E_vectors = vector_zip(E, E_);
        let B_vectors = vector_zip(B, B_);
        let values = vec![Y_vector]
            .into_iter()
            .chain(E_vectors.into_iter())
            .chain(B_vectors.into_iter())
            .collect();
        vec![VectorTuple { values }]
    }
    /**
    Create a ZkPLMT tuple for the proof pi_2
    */
    fn setup_x_proof_tuple(
        A: &CurvePointList,
        X: &CurvePointList,
        B_: &CurvePointList,
        Q_: &CurvePointList,
    ) -> Vec<VectorTuple> {
        let A_vectors = vector_zip(A, X);
        let B_vectors = vector_zip(B_, Q_);
        let values = A_vectors.into_iter().chain(B_vectors.into_iter()).collect();
        vec![VectorTuple { values }]
    }

    /**
    Creates a VerifiableEncryptionOfCurvePoint from the inputs.
    Does not include a proof that (Y,P_) has the slope equal to the private key
    of the signer.

    Let an output TxO be (V[t],A[t],B[t]). Let the public key of the signer be $P$.
    The public key is simply encrypted using the El-Gamal encryption with the public key $(A[t],B[t])$.
    To do that, first a random $x$ is chosen from $𝔽q$, and then the encryption is computed as $E[t]=xB[t]+P$.
    Both $X[t]=xA[t]$ and $E[t]$ are stored in the transaction. The reciever knows the private key of the receiver (say $q[t]$);
    so, the receiver can compute the value of $P$ as $E[t]-q[t]X[t]$. Notice that $B[t]=q[t]A[t]$, so $q[t]X[t]=q[t]xA[t] = xB[t]$.
    This means $E[t]-q[t]X[t] = xB[t]+P-xB[t]=P$.

    Now, the trick is to prove that $E$ indeed is equal to $xB[t]+P$ to the validators who don't know $q$ and must not be able to decrypt $E[t]$.
    The trick is to 'promote' everything by some secret random $y$ and then check the sum. We compute $Y=yG,E_[t]=yE[t], P_=yP$.
    We now want to show that $E_=xyB[t]+P_$, which would then prove that $E[t]=xB[t]+P$. To do this, we must also compute the value of $xyB[t]$.
    We first compute $B_[t]=yB_t$ and then $Q_[t]=xB_[t] = xyB[t]$. To prove the promotion is correct, we compute a a ZkPLMT pi_1
    for the linear tuple $(G,Y),(E[t],E_[t]),(B_[t],yB[t])$. We also compute a ZkPLMT pi_2 to prove the linearity of the tuple (A[t],X[t]),(B_[t],Q_[t])$.
    And finally, we add the pair $(Y,P_)$ to the main ZkPLMT to prove that $P_=pY=yP$.
    $\pi_1$ proves that $B_[t]=yB[t]$ and $E_[t]=yE[t]$. $\pi_2$ proves $Q_[t]=xB_[t] = xyB[t]$. The main signature proves that $P_=yP$.
    Now, the verifier can simply check whether $E_[t]=Q_[t]+P_$ to know whether the encryption is correctly done.
    */
    pub fn create<R>(
        signer_public_perm_P: &RistrettoPoint,
        target_ephemeral_pub_A_B: &[CurveVector],
        generator_point: &RistrettoPoint,
        csprng: &mut R,
    ) -> Result<VerifiableEncryptionOfCurvePoint, ZkPlmtError>
    where
        R: RngCore + CryptoRng,
    {
        let G = generator_point;
        let (A, B) = vector_unzip(target_ephemeral_pub_A_B);
        let x = Scalar::random(csprng);
        let X = &A * x;
        let y = Scalar::random(csprng);
        let Y = y * G;
        let E = &B * x + signer_public_perm_P;
        let P_ = y * signer_public_perm_P;
        let E_ = &E * y;
        let B_ = &B * y;
        let Q_ = &B * x * y;
        let tuples_1 = Self::setup_y_proof_tuple(G, &Y, &E, &E_, &B, &B_);

        let pi_1 = create_zkplmt(&[0u8; 0], &tuples_1, 0, y, csprng)?;

        let tuples_2 = Self::setup_x_proof_tuple(&A, &X, &B_, &Q_);
        let pi_2 = create_zkplmt(&[0u8; 0], &tuples_2, 0, x, csprng)?;
        Ok(VerifiableEncryptionOfCurvePoint {
            Y,
            E: E.0,
            E_: E_.0,
            X: X.0,
            B_: B_.0,
            Q_: Q_.0,
            P_,
            pi_1,
            pi_2,
        })
    }

    /**
    Verifies a VerifiableEncryptionOfCurvePoint from the inputs.
    Does not verify a proof that (Y,P_) has the slope equal to the private key
    of the signer.
    */
    pub fn verify(
        &self,
        target_ephemeral_pub_A_B: &[CurveVector],
        generator_point: &RistrettoPoint,
    ) -> Result<(), EncryptionVerificationError> {
        let G = generator_point;
        let (A, B) = vector_unzip(target_ephemeral_pub_A_B);
        let tuples_1 = Self::setup_y_proof_tuple(
            G,
            &self.Y.clone(),
            &self.E.clone().into(),
            &self.E_.clone().into(),
            &B,
            &self.B_.clone().into(),
        );
        let tuples_2 = Self::setup_x_proof_tuple(
            &A,
            &self.X.clone().into(),
            &self.B_.clone().into(),
            &self.Q_.clone().into(),
        );

        for i in 0..self.Q_.len() {
            let e_ = self.E_[i];
            let q_ = self.Q_[i];
            if e_ != self.P_ + q_ {
                return Err(EncryptionVerificationError::VerificationError);
            }
        }

        verify_zkplmt(&[0u8; 0], &tuples_1, &self.pi_1)?;
        verify_zkplmt(&[0u8; 0], &tuples_2, &self.pi_2)?;
        Ok(())
    }

    pub fn decrypt(&self, index: usize, private_key: Scalar) -> RistrettoPoint {
        decrypt(self.E[index], self.X[index], private_key)
    }

    #[cfg(any(test, feature = "test-helpers"))]
    pub fn random<R>(csprng: &mut R) -> Self
    where
        R: RngCore + CryptoRng,
    {
        let signer_key = RistrettoPoint::random(csprng);
        let target = [CurveVector {
            x: RistrettoPoint::random(csprng),
            y: RistrettoPoint::random(csprng),
        }];
        Self::create(&signer_key, &target, &RISTRETTO_BASEPOINT_POINT, csprng)
            .expect("This should not fail as long as target has at least one curve vector")
    }
}

/// Used to decrypt signer identity given an X and E
pub fn decrypt(e: RistrettoPoint, x: RistrettoPoint, private_key: Scalar) -> RistrettoPoint {
    e - private_key * x
}

#[cfg(test)]
#[allow(non_snake_case)]
mod tests {
    use rand::rngs::OsRng;

    use super::*;
    use crate::core::get_random_curve_point;
    use curve25519_dalek::constants::RISTRETTO_BASEPOINT_POINT;

    const G: RistrettoPoint = RISTRETTO_BASEPOINT_POINT;

    #[test]
    fn test_positive() {
        let mut csprng: OsRng = OsRng::default();
        let target_ephemeral_pub_A_B: Vec<CurveVector> = (1..5)
            .map(|_| CurveVector {
                x: get_random_curve_point(&mut csprng),
                y: get_random_curve_point(&mut csprng),
            })
            .collect();
        let signer_public_perm_P = get_random_curve_point(&mut csprng);
        let ve = VerifiableEncryptionOfCurvePoint::create(
            &signer_public_perm_P,
            &target_ephemeral_pub_A_B,
            &G,
            &mut csprng,
        )
        .expect("Failed to create proof");
        assert!(ve.verify(&target_ephemeral_pub_A_B, &G).is_ok());
    }

    #[test]
    fn test_fail_change_target_key() {
        let mut csprng: OsRng = OsRng::default();
        let target_ephemeral_pub_A_B: Vec<CurveVector> = (1..5)
            .map(|_| CurveVector {
                x: get_random_curve_point(&mut csprng),
                y: get_random_curve_point(&mut csprng),
            })
            .collect();

        let different_target_ephemeral_pub_A_B: Vec<CurveVector> = (1..5)
            .map(|_| CurveVector {
                x: get_random_curve_point(&mut csprng),
                y: get_random_curve_point(&mut csprng),
            })
            .collect();
        let signer_public_perm_P = get_random_curve_point(&mut csprng);
        let ve = VerifiableEncryptionOfCurvePoint::create(
            &signer_public_perm_P,
            &target_ephemeral_pub_A_B,
            &G,
            &mut csprng,
        )
        .expect("Failed to create proof");
        assert!(ve.verify(&different_target_ephemeral_pub_A_B, &G).is_err());
    }

    #[test]
    fn test_fail_change_Y() {
        let mut csprng: OsRng = OsRng::default();
        let target_ephemeral_pub_A_B: Vec<CurveVector> = (1..5)
            .map(|_| CurveVector {
                x: get_random_curve_point(&mut csprng),
                y: get_random_curve_point(&mut csprng),
            })
            .collect();

        let signer_public_perm_P = get_random_curve_point(&mut csprng);
        let mut ve = VerifiableEncryptionOfCurvePoint::create(
            &signer_public_perm_P,
            &target_ephemeral_pub_A_B,
            &G,
            &mut csprng,
        )
        .expect("Failed to create proof");
        ve.Y = get_random_curve_point(&mut csprng);
        assert!(ve.verify(&target_ephemeral_pub_A_B, &G).is_err());
    }

    #[test]
    fn test_fail_change_P_() {
        let mut csprng: OsRng = OsRng::default();
        let target_ephemeral_pub_A_B: Vec<CurveVector> = (1..5)
            .map(|_| CurveVector {
                x: get_random_curve_point(&mut csprng),
                y: get_random_curve_point(&mut csprng),
            })
            .collect();

        let signer_public_perm_P = get_random_curve_point(&mut csprng);
        let mut ve = VerifiableEncryptionOfCurvePoint::create(
            &signer_public_perm_P,
            &target_ephemeral_pub_A_B,
            &G,
            &mut csprng,
        )
        .expect("Failed to create proof");
        ve.P_ = get_random_curve_point(&mut csprng);
        assert!(ve.verify(&target_ephemeral_pub_A_B, &G).is_err());
    }

    #[test]
    fn test_decryption() {
        let mut csprng: OsRng = OsRng::default();
        let private_key = Scalar::random(&mut csprng);
        let target_ephemeral_pub_A_B: Vec<CurveVector> = (1..5)
            .map(|_| {
                let x = get_random_curve_point(&mut csprng);
                let y = private_key * x;
                CurveVector { x, y }
            })
            .collect();
        let signer_public_perm_P = get_random_curve_point(&mut csprng);
        let ve = VerifiableEncryptionOfCurvePoint::create(
            &signer_public_perm_P,
            &target_ephemeral_pub_A_B,
            &G,
            &mut csprng,
        )
        .expect("Failed to create proof");
        ve.verify(&target_ephemeral_pub_A_B, &G)
            .expect("Failed to verify proof");

        let dectryped_P = ve.decrypt(0, private_key);
        assert_eq!(dectryped_P, signer_public_perm_P);
    }
}
