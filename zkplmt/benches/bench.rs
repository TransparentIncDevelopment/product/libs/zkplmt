mod verifiable_encryption;

use criterion::criterion_main;
use verifiable_encryption::verifiable_encryption;

criterion_main!(verifiable_encryption);
